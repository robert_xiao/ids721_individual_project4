# IDS721_Individual_project4


### Requirments
- Rust AWS Lambda function. <br>
- Step Functions workflow coordinating Lambda. <br>
- Orchestrate data processing pipeline. <br>
### Rust Lambda Functionality
ProcessTransaction function is to receive a string of "TransactionType", and determine the TransactionType is PURCHASE or REFUND, if it is PURCHASE, then jump to the ProcessPurchase function stage. If it is REFUND, then jump to the ProcessRefund stage. ProcessRefund function is to receive a string containing the number of items you want to return and price information and output the total amount of refund. ProcessPurchase function is to receive a string containing the number of items you want to buy and price information. The ProcessPurchase function takes a string containing the quantity and price of the item you want to purchase and outputs the total amount of the purchase. <br>
Steps
1. To create a lambda project, use {cargo lambda new <PROJECT_NAME>}.<br>
2. Include all required dependencies in `Cargo.toml.<br>
3. Expand the features in `main.rs. `<br>
4. Next, use the identical procedures as before to generate the second lambda function.<br>
5. Next, use the following command to construct the lambda function:    <br>
```
cargo lambda build --release

```
6. Next, use the following command to deploy the lambda function to AWS Lambda:<br>
```
cargo lambda deploy --region <REGION> --iam-role <ROLE_ARN>

```
7. Then, using the identical procedures as before, deploy the second lambda function. <br>
8. Using Amazon Step Functions to create a new State Machine. <br>
9. Choose AWS Lambda Invoke and drag it to the state machine with the order of the Three functions to implement the step function's workflow. My state machine consequently is like the following.<br>
![Alt text](image.png)<br>
 
10. Run the state machine to verify that the functions are performed in the correct order and that the desired result is produced.<br>

### Demo Video

The video is called DEMO_Individual_project4.mp4. It is located in this repository's root directory.<br>

### Srceenshots
#### AWS Lambda Functions
   ![Alt text](image-1.png)<br>
   ![Alt text](image-2.png)<br>
#### Input:
```
{
  "TransactionType": "REFUND",
  "ProductType": "Apples, Grapes and Pears",
  "GoodsPrice": "3,5,2",
  "GoodsNum": "1,2,3"
}

```

```
{
  "TransactionType": "PURCHASE",
  "ProductType": "Apples, Grapes and Pears",
  "GoodsPrice": "4,3,7",
  "GoodsNum": "3,5,2"
}

```
#### Execution Results
![Alt text](image-3.png)<br>
![Alt text](image-4.png)<br>
#### Execution Events
![Alt text](image-5.png)<br>
![Alt text](image-6.png)<br>