from __future__ import print_function
import json
import urllib
import boto3
import datetime

print('Loading function...')



    
def process_refund(message,context):
    print("Calculating total price...")
    # print(f"Quantity String: {goods_num_str}, Price String: {goods_price_str}")
    

    goods_num_list = message["GoodsPrice"].split(',')
    goods_price_list = message["GoodsNum"].split(',')

    total_price = 0
    for num, price in zip(goods_num_list, goods_price_list):
        total_price += int(num) * float(price)
    
    
    print("Received message from Step Functions:")
    print(message)
    
    
    response = {}
    response["TransactionType"] = message["TransactionType"]
    response["Timestamp"] = datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S")
    response["Message"] = "I am the sales clerk and I will refund you for the following items"
    response["Goods"] = message["ProductType"]
    response["Price"] = message["GoodsPrice"]
    response["Number"] = message["GoodsNum"]
    response["TotalPrice"] = f"{total_price}$"
    
    
    return response

# def lambda_handler(event, context):
#     # TODO implement
#     return {
#         'statusCode': 200,
#         'body': json.dumps('Hello from Lambda!')
#     }


# def lambda_handler(event, context):
#     # TODO implement
#     return {
#         'statusCode': 200,
#         'body': json.dumps('Hello from Lambda!')
#     }
